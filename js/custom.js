//books swiper
var swiper = new Swiper('.booksList', {
  slidesPerView: 1,
  spaceBetween: 10,
  autoplay: {
    delay: 5000,
    disableOnInteraction: true,
  },
  // init: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 40,
    },
    1024: {
      slidesPerView: 5,
      spaceBetween: 10,
    },
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});




var swiper = new Swiper('.homeSliderSwiper', {
  spaceBetween: 30,
  effect: 'fade',
  pagination: {
    // el: '.swiper-pagination',
    clickable: true,
  },
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  }, 
});





  //header add class
  $(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $("header").addClass("fixed-top");
    } else {
        $("header").removeClass("fixed-top");
    }
  });

  
     