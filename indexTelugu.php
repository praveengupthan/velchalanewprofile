<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dr Velchala Kondal Rao</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
    <?php include 'includes/styles.php'?>
    <?php include 'includes/arrayObjects.php'?>
    
</head>
<body class="teluguSite">

    <!-- header -->
    <header>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="#"> <img src="img/logo.svg"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link tfont" href="http://shop.velchala.com/publications" target="_blank">పుస్తక గ్రంధాలయం </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link navBtn" href="index.php">English</a>
                        </li>
                    </ul>                   
                </div>
            </na>                
        </div>
    </header>
    <!--/ header -->

    <!-- main -->
    <!-- slider -->
    <div class="homeSlider">
        <!-- container -->
        <div class="container-fluid pl-0">
            <!-- row -->
            <div class="row">
                <!-- left slider col -->
                <div class="col-md-6 pl-0 pr-0 pr-sm-3">
                    <!-- Swiper -->
                    <div class="swiper-container homeSliderSwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide" style="background-image:url(./img/slider01.jpg)"></div>
                            <div class="swiper-slide" style="background-image:url(./img/slider02.jpg)"></div>
                            <div class="swiper-slide" style="background-image:url(./img/slider03.jpg)"></div>
                        </div>
                        <!-- Add Pagination -->
                        <!-- <div class="swiper-pagination swiper-pagination-white"></div>                        -->
                    </div>
                </div>
                <!--/ left slider col -->
                
                <!-- right col -->
                <div class="col-md-6 align-self-center sliderArticle tfont">
                    <article>
                        <h1 class="tfont">సుస్వాగతం  <br>డాక్టర్  వెల్చాల కొండల్ రావు </h1>
                        <p>తెలుగు అకాడెమీ మాజీ సంచాలకులు,'జయంతి' సాహిత్య సాంస్కృతిక త్రైమాసిక పత్రిక సంపాదకులు, పోయెట్రీ సొసైటీ అఫ్ ఇండియా జీవిత సభ్యులు, పోయెట్రీ సొసైటీ అఫ్ హైదరాబాద్ సభ్యులు, ఇండియన్ ఇన్స్టిట్యూట్ అఫ్ పబ్లిక్ అడ్మినిస్ట్రేషన్ పర్మనెంట్ సభ్యులు, ఫోరమ్ ఫర్ హయ్యర్ ఎడ్యుకేషన్, హైదరాబాద్ ఉపాధ్యక్షులు, తెలుగు,ఇంగ్లీష్, ఉర్దూ భాషల్లో రచయిత, అనువాదకుడు, సంకలనకర్త.</p>
                        <a href="http://shop.velchala.com/publications" target="_blank" class="orangeBtn tfont">పుస్తక గ్రంధాలయం సందర్శించండి</a>
                    </article>
                </div>
                <!--/ right col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ slider -->

    <!-- vsp block -->
    <div class="vspSection">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
               
                <!-- col -->
                <div class="col-md-8 align-self-center tfont">
                    <article>
                        <h1 class="tfont">స్థాపకులు  <br>విశ్వనాథ సాహిత్య పీఠం</h1>
                        <p class="tfont">విశ్వనాథ సత్యనారాయణ పేరుతో "విశ్వనాథ సాహిత్య పీఠం" జులై 2003లో ప్రారంభమైంది. కవులు, రచయితల పేరు మీద సాహితీ సంస్థలు, పీఠాలు నెలకొల్పడం సహజమే. అయితే విశ్వనాథ మరణం తరువాత మూడు దశాబ్దాలకు ఈ పీఠం ఏర్పడడం వెనుక దాదాపు మూడు దశాబ్దాల పాటు సాగిన సుదీర్ఘ అంతర్మథన, ఆలోచనల, సంప్రదింపుల సమాహారం ఉంది.</p>                        
                    </article>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-md-4">
                    <img src="img/vsp-png.png" alt="" class="img-fluid">
                </div>
                <!--/ col -->               
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
        <!-- vsp header -->
        <div class="vspHeader">
            <div class="container-fluid">
                <img src="img/kavi_samraat.jpg" alt="">
                <h3 class="text-uppercase fbold tfont">ఎ విజన్ అండ్ ప్రొజెక్షన్</h3>
                <p class="text-center">
                    <i>
                        "I am convinced also of this, the heart that must rule humanity is replaced by the mind. It is a bane"
                    </i>
                </p>
            </div>
        </div>
        <!--/ vsp header -->
        <!-- vsp body -->
        <div class="container">
            <div class="row pt-3">
                <div class="col-md-6">
                    <p class="tfont">విశ్వనాథ రచనలను సమాజంలో అందరికి అందుబాటులోకి తేవటం, దీనికనుగుణంగా సాహిత్య, సాంస్కృతిక కార్యక్రమాలు చేపట్టడం.  విశ్వనాథ గ్రంధాలయ స్థాపన: ఇందులో విశ్వనాథ రచనలు, లేఖలు, ప్రసంగాలు, ఇంటర్వ్యూలతో పాటు వారి రచనలపై వఛ్చిన పరిశోధనలు, విమర్శలు, కావ్యాలు, ప్రసంశలు, పరిశోధకులకు సహాయ ప్రొత్సాహాలందిచ్ఛే ప్రధానాంశాలతో నిక్షేపించడం.</p>
            
                    <p class="tfont">విశ్వనాథ రచనలు ఇతర భాషల్లోకి అనువదింపచేయడం.  ప్రతి సంవత్సరం తెలుగులో ఒక రచయిత/రచయిత్రిని విశ్వనాథ సాహిత్య పురస్కారంతో సన్మానించడం. దీనికోసం ప్రతిభా నిర్ణాయక సంఘాన్ని స్థాపించి ఎంపిక చేయడం. జయంతి' అన్న పేరున ఒక త్రైమాసిక సాహిత్య పత్రిక ప్రచురించటం, రచనల కొరకు సాహిత్యంలో పోటీలు, గోష్టులు, సమీక్షలు, సమావేశాలు నిర్వహించి వ్యాసాలు వగైరా ఎంపిక చేయడం. విశ్వనాథ రచనల పఠనా అవగాహన కొరకు ఏడాదికొకసారి రాష్ట్రంలోని వివిధ ప్రాంతాల్లో, సాహిత్య సాంస్క్రితిక సమావేశాలు, సభలు నిర్వహించడం.</p>
                </div>
                <div class="col-md-6">
                    <p class="tfont">ఏడాదికొకసారి విశ్వనాథ సాహిత్య సృష్టిలో ఏదో ఒక ప్రక్రియపై సాహిత్య గోష్టి ఏర్పాటు ఏర్పాటు చేయటం. విశ్వనాథ సాహితీ ప్రక్రియల్లో ఏదో ఒకదానిపై ఏడాదికొకసారి వ్యాస రచన పోటీలు నిర్వహించి వారి జన్మదినోత్సవం నాడు ఉత్తమ వ్యాస రచయిత/రచయిత్రిని సన్మానించడం. విశ్వనాథ రచనల్లో ఏదో ఒక రచనపై నెలకొకసారి ఉపన్యాస కార్యక్రమం హైద్రాబాద్లో కానీ, వేరేచోట కానీ నిర్వహించడం. </p>
            
                    <p class="tfont">"కవిసామ్రాట్ విశ్వనాథ సత్యనారాయణ డీమ్డ్ యూనివర్సిటీ ఫర్ కల్చరల్ స్టడీస్" అన్నదానిని హైద్రాబాద్ లో స్థాపించడం. విశ్వనాథ విగ్రహాన్నితదితర మహనీయుల ప్రక్కన హైద్రాబాద్లోని ట్యాంకుబండు వద్ద నెలకొల్పుటకు "రెప్రెసెంత్" చేయడం. ప్రతియేట ప్రసిద్ధుడైన ఒక పండితుణ్ణి ఆహ్వానించి విశ్వనాథ వారి జన్మదినం రోజున విశ్వనాథ స్మారక ఉపన్యాసం ఇప్పించడం. ఈ లక్ష్యాల సాధన కొరకు విశ్వనాథ సాహిత్య పీఠాన్ని స్థాపించడం జరిగింది.</p>
                </div>
            </div>
        </div>
        <!--/ vsp body -->
    </div>
    <!--/ vsp block -->

    <!-- career-->
    <div class="careerSection">
        <!-- container -->
        <div class="container">
            <div class="row">
                <article class="careerHeading">
                    <h2 class="tfont">75 సంవత్సరాలు </h2>
                    <h1 class="tfont">చరిత్ర  &amp; ప్రయాణం </h1>
                    <p class="fbold tfont">1932 జూలై 21 న తెలంగాణలోని కరీంనగర్‌లో జన్మించారు</p>
                </article>                
            </div>

            <div class="row">
                <!-- col -->
                <div class="col-md-12">
                    <article class="careerArticle">
                        <h2 class="tfont">లైఫ్ టైం అచివ్మెంట్ అవార్డు గ్రహీత</h2>
                        <p class="tfont">సమర్థవంతమైన విద్యావేత్త మరియు గొప్ప రచయిత డాక్టర్ వెల్చల కొండల్ రావు, 2015 ఏప్రిల్ 20 న హైదరాబాద్ లోని రవీంద్ర భారతిలో జరిగిన కార్యక్రమంలో జీవిత భాషా సాంస్కృతిక పురస్కారాన్ని సత్కరించింది.</p>
                    </article>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-md-6">
                    <article class="careerArticle">
                        <h2 class="tfont">వ్యక్తిగత వివరాలు </h2>
                        <p class="tfont">1932 జూలై 21 న తెలంగాణలోని కరీంనగర్‌లో జన్మించారు. వి.మిర్మల దేవిని వివాహం చేసుకున్నారు. పి. రామదేవి మరియు ఆర్. ఉమా దేవి మరియు ఒక కుమారుడు అగం రావు వెల్చాలా అనే ఇద్దరు కుమార్తెలతో ఆశీర్వదించారు
                        </p>
                    </article>
                    <article class="careerArticle">
                        <h2 class="tfont">ఎడిటర్ - పుస్తకాల కంపైలర్</h2>
                        <p class="tfont">ఇంగ్లీష్ మరియు తెలుగు భాషలలో 10 పుస్తకాలను సవరించారు, "తెలంగాణ పోరాటం కోసం గుర్తింపు" అనే పుస్తకంతో సహా.
                        </p>
                    </article>
                    <article class="careerArticle">
                        <h2 class="tfont">విద్యా నేపథ్యం</h2>
                        <p class="tfont">ఉస్మానియా విశ్వవిద్యాలయం నుండి ఎం. కాం. .</p>
                        <p class="tfont">లక్నో విశ్వవిద్యాలయం నుండి ఎల్.ఎల్.బి. </p>
                    </article>
                    <article class="careerArticle">
                        <h2 class="tfont">కవి </h2>
                        <ul class="listItems tfont">
                            <li>తెలుగు </li>
                            <li>ఇంగ్లీష్ </li>
                            <li>ఉర్దూ </li>
                            <li>పై నుండి మూడు పుస్తకాలను అనువదించారు</li>
                        </ul>        
                    </article>
                    <article class="careerArticle">
                        <h2 class="tfont">భారత స్వతంత్ర సమర యోధులు</h2>
                        <ul class="listItems tfont">
                            <li>భారత స్వాతంత్ర పోరాటం మరియు హైదరాబాద్ విముక్తి పోరాటంలో పనిచేశారు</li>
                            <li>1947 ఆగస్టు 7 న హైదరాబాద్ లోని కోటిలో సత్యాగ్రహాన్ని ప్రదర్శించారు</li>
                            <li>హైదరాబాద్‌లోని చంచల్‌గుడ జైలులో 7 రోజులు జైలు శిక్ష అనుభవించారు</li>
                            <li>హైదరాబాద్ రాష్ట్రంలోని ఆసిఫాబాద్ జైలులో 6 నెలలు జైలు శిక్ష అనుభవించారు</li>
                            <li>భారత స్వతంత్రసమరం లో పాల్గున్నందుకు ప్రభుత్వం నుండి తామర పత్రాన్ని అందుకున్నారు</li>
                            <li>భారతదేశం ప్రభుత్వం ఫ్రీడమ్ ఫైటర్ పెన్షన్ గ్రహీత</li>
                        </ul>
                    </article>
                    <article class="careerArticle">
                        <h2 class="tfont">అవార్డులు</h2>
                        <ul class="listItems tfont">
                            <li>ఎన్టీఆర్ తెలుగు విశ్వ విద్యాలయం నుండి గౌరవ డాక్టరేట్</li>
                            <li>కాలిఫోర్నియాలోని ఇంటర్నేషనల్ అకాడమీ ఆఫ్ ఆర్ట్స్ నుండి గౌరవ డాక్టరేట్</li>
                            <li>కోల్‌కతాలోని మైఖేల్ మధుసూదన్ దత్ అకాడమీ నుండి గౌరవ డాక్టరేట్</li>
                            <li>సనతన ధర్మ ఛారిటబుల్ ట్రస్ట్ ఆ సద్గురు శివానంద మూర్తి నుండి ప్రతిభ పురస్కారం</li>
                            <li>బాల్ గంగాధర్ తిలక్ కు చెందిన అమృతం కురిసినా రాత్రిని అనువదించినందుకు ఎన్.టి.ఆర్ తెలుగు విశ్వవిద్యాలయం, హైదరాబాద్ నుండి మొదటి బహుమతి అందుకున్నారు</li>
                            <li>దేవులపల్లి రామానుజా రావు అవార్డు అఫ్ తెలంగాణ సరస్వతి పరిషత్</li>
                        </ul>
                    </article>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-md-6">
                    <article class="careerArticle">
                        <h2 class="tfont">విద్యావేత్త & నిర్వాహకుడు</h2>
                        <ul class="listItems tfont">
                            <li>మాజీ ప్రిన్సిపాల్, ప్రభుత్వం డిగ్రీ కళాశాల, జగిత్యాల్, AP</li>
                            <li>మాజీ ప్రిన్సిపాల్, ఎస్.ఆర్.ఆర్. ప్రభుత్వం డిగ్రీ కళాశాల, కరీంనగర్, తెలంగాణ</li>
                            <li>మాజీ వ్యవస్థాపక ప్రిన్సిపాల్, మహిళా కళాశాల, కరీంనగర్</li>
                            <li>మాజీ ప్రిన్సిపాల్, ప్రభుత్వ నగర కళాశాల, హైదరాబాద్</li>
                            <li>మాజీ కోఆర్డినేటర్, ఎన్టీఆర్ తెలుగు విశ్వవిద్యాలయం</li>
                            <li>మాజీ స్పెషల్ ఆఫీసర్, ఇన్స్టిట్యూట్ ఆఫ్ ప్రొఫెషనల్ స్టడీస్, ఎన్.టి. రామారావు</li>
                            <li>మాజీ డైరెక్టర్, శాతవాహన ఇన్స్టిట్యూట్ ఆఫ్ పోస్ట్ గ్రాడ్యుయేట్ స్టడీస్, కరీంనగర్</li>
                            <li>మాజీ సభ్యుడు, ది సిండికేట్ ఆఫ్ ఉస్మానియా విశ్వవిద్యాలయం, హైదరాబాద్</li>
                            <li>మాజీ జాయింట్ డైరెక్టర్, ఉన్నత విద్య, ప్రభుత్వం ఆంధ్రప్రదేశ్</li>
                            <li>మాజీ డైరెక్టర్, తెలుగు అకాడమీ, ఆంధ్రప్రదేశ్</li>
                            <li>మాజీ వ్యవస్థాపక ఛైర్మన్, సిస్టర్ నివేదా కాలేజ్ ఆఫ్ ప్రొఫెషనల్ స్టడీస్, హైదరాబాద్</li>
                            <li>వ్యవస్థాపకుడు గౌరవ. ఛైర్మన్, తెలంగాణ సాహిత్య, విద్యా, సాంస్కృతిక వేదిక</li>
                            <li>ఫౌండర్ చైర్మన్, తెలుగు భాష పరిరక్షణ సమితి, తెలంగాణ</li>
                            <li>ప్రస్తుతం గౌరవప్రద. ఛైర్మన్, సిస్టర్ నివేదిత స్కూల్, హైదరాబాద్</li>                            
                        </ul>
                    </article>

                    <article class="careerArticle">
                        <h2 class="tfont">జర్నలిస్ట్</h2>
                        <ul class="listItems tfont">
                            <li>ఫార్మర్ ఎడిటర్ - శాతవాహన జర్నల్ ఆఫ్ శాతవాహన ఇన్స్టిట్యూట్ ఆఫ్ పోస్ట్ గ్రాడ్యుయేట్ స్టడీస్, కరీంనగర్</li>
                            <li>మాజీ ఎడిటర్ - ఆంధ్రప్రదేశ్ లోని తెలుగు అకాడమీ</li>
                            <li>మాజీ ఎడిటర్ - బిజినెస్ విజన్, ఇంగ్లీష్</li>
                            <li>ప్రస్తుత ఎడిటర్ - జయంతి, తెలుగు త్రైమాసికం</li>                                           
                        </ul>
                    </article>

                    <article class="careerArticle">
                        <h2 class="tfont">గౌరవ. పదవులు</h2>
                        <ul class="listItems tfont">
                            <li>హైదరాబాద్ పోయెట్రీ సొసైటీ మాజీ అధ్యక్షుడు</li>
                            <li>హైదరాబాద్ ఇంటర్ భాషా కవితల సంఘం వ్యవస్థాపక అధ్యక్షుడు</li>
                            <li>హైదరాబాద్ తెలంగాణ ఎడ్యుకేషనల్ అండ్ కల్చరల్ ఫోరం వ్యవస్థాపకుడు కన్వీనర్</li>
                            <li>డిల్లీ లోని పోయెట్రీ సొసైటీ ఆఫ్ ఇండియా శాశ్వత సభ్యుడు</li>
                            <li>డిల్లీ లోని ఇండియన్ ఇన్స్టిట్యూట్ ఆఫ్ పబ్లిక్ అడ్మినిస్ట్రేషన్ యొక్క శాశ్వత సభ్యుడు</li>
                            <li>ఫోరమ్ ఫర్ హయ్యర్ ఎడ్యుకేషన్ మాజీ వైస్ చైర్మన్, హైదరాబాద్</li>
                            <li>ప్రస్తుతం హైదరాబాద్ విశ్వనాథ సాహిత్య పీఠం ఛైర్మన్</li>
                        </ul>
                    </article>
                </div>
                <!--/ col -->
            </div>
        </div>
        <!--/ container -->
    </div>
    <!--/ career -->

    <!-- books -->
    <div class="booksVelchala">
        <!-- container -->
        <div class="container">
             <article>
                <h1 class="tfont">పుస్తకాలు <br>డా. వెల్చల కొండల్ రావు</h1>
                <p class="fwhite tfont">36 Books written in Telugu, English, Urdu</p>                        
            </article>
            <!-- books swiper -->
                <div class="booksSwiper">
                    <!-- Swiper -->
                    <div class="swiper-container booksList">
                        <div class="swiper-wrapper">
                            <?php 
                            for ($i=0; $i<count($bookSlider);$i++){ ?>
                            <div class="swiper-slide">
                                <img src="img/books/<?php echo $bookSlider[$i][0]?>.jpg" alt="" class="img-fluid"> 
                            </div>
                            <?php } ?>                                               
                        </div>
                        <!-- Add Pagination -->
                        <!-- <div class="swiper-pagination"></div> -->
                        <!-- Add Arrows -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                     <p class="pt-5 text-center">
                        <a href="http://shop.velchala.com" target="_blank" class="orangeBtn tfont">అన్ని పుస్తకాలను చూడండి</a>
                    </p>   
                </div>
                <!--/ books swiper -->
        </div>
        <!--/ container -->
    </div>
    <!--/ books -->

     <!-- video -->
    <div class="videoSection py-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <iframe frameborder="0" width="100%" height="600px" src="https://youtube.com/embed/WRBTxGCYxHo?autoplay=1&amp;mute=1&amp;loop=1&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;autohide=1&amp;playlist=WRBTxGCYxHo" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--/ video -->
    <!--/ main -->

    <!-- footer -->
    <footer>
       <ul class="nav justify-content-center socialnav">
            <li class="nav-item">
                <a href="https://www.facebook.com/" target="_blank" class="nav-link"><span class="icon-facebook icomoon"></span></a>
            </li>
            <li class="nav-item">
                <a href="https://twitter.com/" target="_blank" class="nav-link"><span class="icon-twitter icomoon"></span></a>
            </li>
            <li class="nav-item">
                <a href="https://www.linkedin.com/" target="_blank" class="nav-link"><span class="icon-linkedin icomoon"></span></a>
            </li>
        </ul>
        <p>© 2021 Velchala Kondal Rao. All rights reserved.</p>
    </footer>
    <!--/ footer -->
    <?php include 'includes/footer.php'?>

   
</body>
</html>
