<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dr Velchala Kondal Rao</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
    <?php include 'includes/styles.php'?>
    <?php include 'includes/arrayObjects.php'?>
    
</head>
<body>
    <!-- header -->
    <header>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="#"> <img src="img/logo.svg"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="http://shop.velchala.com/publications" target="_blank">Book Store</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link navBtn" href="indexTelugu.php">తెలుగు</a>
                        </li>
                    </ul>                   
                </div>
            </na>                
        </div>
    </header>
    <!--/ header -->

    <!-- main -->
    <!-- slider -->
    <div class="homeSlider">
        <!-- container -->
        <div class="container-fluid pl-0">
            <!-- row -->
            <div class="row">
                <!-- left slider col -->
                <div class="col-md-6 pl-0 pr-0 pr-sm-3">
                    <!-- Swiper -->
                    <div class="swiper-container homeSliderSwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide" style="background-image:url(./img/slider01.jpg)"></div>
                            <div class="swiper-slide" style="background-image:url(./img/slider02.jpg)"></div>
                            <div class="swiper-slide" style="background-image:url(./img/slider03.jpg)"></div>
                        </div>
                        <!-- Add Pagination -->
                        <!-- <div class="swiper-pagination swiper-pagination-white"></div>                        -->
                    </div>
                </div>
                <!--/ left slider col -->
                
                <!-- right col -->
                <div class="col-md-6 align-self-center sliderArticle">
                    <article>
                        <h1>Welcome to <br>velchala Kondal  Rao</h1>
                        <p>Dr. Rao is basically an educationist, known for his pioneering mind and promotinal drive and initiative. He has been the  architect of many educational institutions in Andhra Pradesh. Left an indelible mark wherever he worked and whatever positions he held. The last and the last but one positions he held in the State of Andhra Pradesh were Director, telugu  Academy and Joint Director, Higher Education.</p>
                        <a href="http://shop.velchala.com/publications" target="_blank" class="orangeBtn">Visit Book Store</a>
                    </article>
                </div>
                <!--/ right col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ slider -->

    <!-- vsp block -->
    <div class="vspSection">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
               
                <!-- col -->
                <div class="col-md-8 align-self-center">
                    <article>
                        <h1>Founder <br>Viswanatha Sahitya Peetham</h1>
                        <p>A literary cum cultual organization namely "Vishwanatha Shitya Peetham" is established by "Sister Nivedita Foundation" at Red Hills, Hyderabad to function with effect from the month of july 2003.</p>                        
                    </article>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-md-4">
                    <img src="img/vsp-png.png" alt="" class="img-fluid">
                </div>
                <!--/ col -->               
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
        <!-- vsp header -->
        <div class="vspHeader">
            <div class="container-fluid">
                <img src="img/kavi_samraat.jpg" alt="">
                <h3 class="text-uppercase fbold">A Vision and a Projection</h3>
                <p class="text-center">
                    <i>
                        "I am convinced also of this, the heart that must rule humanity is replaced by the mind. It is a bane"
                    </i>
                </p>
            </div>
        </div>
        <!--/ vsp header -->
        <!-- vsp body -->
        <div class="container">
            <div class="row pt-3">
                <div class="col-md-6">
                    <p>The primary objective of the “Peetam” is to acquaint more people with Viswanadha’s genius and Works by organizing such literary and cultural meets and activities as may be needed. To establish a library with books by and on Viswanadha, including audio video material, letters, radio talks and the like to facilitate the research work of scholars doing research on Viswanadha, and also to arrange such guidance and counselling as may be needed by the research scholars. </p>
            
                    <p>To encourage translations of Viswanadha’s works into other languages. To institute an award namely
                        “Viswanadha Award” to be given to an eminent writer in Telugu adjudged as such by the jury constituted for the purpose. To start a quarterly by name “JAYANTHI“ to publish literary views and reviews. To conduct cultural and literary programs once in a year.</p>
                </div>
                <div class="col-md-6">
                    <p>To hold a seminar once a year on one of the literary aspects of Viswanadha. To hold an essay writing
                        competition once a year on one of Viswanadha’s literary aspects and to award prizes. To hold a meeting once a month at Hyderabad or at any other place to discuss about one of the books of Viswanadha. To help to establish a Deemed University namely “Kavisamrat Viswanadha Satyanarayana Deemed University for Literary and Cultural Studies” at Hyderabad.</p>
            
                    <p>To arrange a Viswanadha Memorial Lecture by an eminent scholar every year on his birthday. For the purpose of achieving the above objectives a literary and cultural organization namely “Viswanatha Sahitya Peetam” for the promotion of Viswanadha’s Thoughts.</p>
                </div>
            </div>
        </div>
        <!--/ vsp body -->
    </div>
    <!--/ vsp block -->

    <!-- career-->
    <div class="careerSection">
        <!-- container -->
        <div class="container">
            <div class="row">
                <article class="careerHeading">
                    <h2>75 Years</h2>
                    <h1>History &amp; Timeline</h1>
                    <p class="fbold">BORN ON 21ST JULY, 1932 AT KARIMNAGAR, TELANGANA</p>
                </article>                
            </div>

            <div class="row">
                <!-- col -->
                <div class="col-md-12">
                    <article class="careerArticle">
                        <h2>Lifetime Achievement Award</h2>
                        <p>Dr Velchala Kondal Rao, an efficient educationist and a prolific writer, was felicitated with Lifetime Achievement Award in a function at Ravindra Bharati, Hyderabad on April 20, 2015 by a literary-cultural organisation Saampradaya in association with the Department of Language & Culture, Government of Telangana.</p>
                    </article>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-md-6">
                    <article class="careerArticle">
                        <h2>Personal Information</h2>
                        <p>Born on 21st July, 1932 at Karimnagar, Telangana, Married to V. Nirmala Devi and Blessed with two daughters, P. Rama Devi and R. Uma Devi and a son Agam Rao Velchala
                        </p>
                    </article>
                    <article class="careerArticle">
                        <h2>EDITOR - COMPILER OF BOOKS</h2>
                        <p>Edited 10 books in English and Telugu including a book on "Telangana Struggle for Identity"
                        </p>
                    </article>
                    <article class="careerArticle">
                        <h2>EDUCATIONAL BACKGROUND</h2>
                        <p>M. Com. from Osmania University</p>
                        <p>LL.B. from University of Lucknow</p>
                    </article>
                    <article class="careerArticle">
                        <h2>POET / TRANSLATOR</h2>
                        <ul class="listItems">
                            <li>Telugu</li>
                            <li>English</li>
                            <li>Urdu</li>
                            <li>Translated 10 books from above Three languages</li>
                        </ul>        
                    </article>
                    <article class="careerArticle">
                        <h2>Indian Freedom Fighter</h2>
                        <ul class="listItems">
                            <li>Worked underground during the Indian Independence Struggle and Hyderabad Liberation struggle</li>
                            <li>Performed Satyagraha on 7th August 1947 at Kothi, Hyderabad</li>
                            <li>Imprisoned for 7 days at Chanchalguda Jail, Hyderabad</li>
                            <li>Imprisoned for 6 months at Asifabad Jail, Hyderabad State</li>
                            <li>Receiver of Tamrapatra from Govt. of India for having participated in freedom struggle</li>
                            <li>Receiver of Freedom Fighter Pension, Govt. of India</li>
                        </ul>
                    </article>
                    <article class="careerArticle">
                        <h2>AWARDS</h2>
                        <ul class="listItems">
                            <li>Hon. Doctorate from N.T.R. Telugu University, Hyderabad</li>
                            <li>Hon. Doctorate from International Academy of Arts, California</li>
                            <li>Hon. Doctorate from Michael Madhusudan Dutt Academy, Kolkata</li>
                            <li>Pratibha Puraskar by Sanatana Dharma Charitable Trust of Sadguru Sivananda Murthy</li>
                            <li>Received first prize for translating Amrutham Kurisina Ratri of Bal Gangadhar Tilak from N.T.R. Telugu University,Hyderabad</li>
                            <li>Recipient of Devulapalli Ramanuja Rao Award of Telangana Saraswathi Parishath</li>
                        </ul>
                    </article>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-md-6">
                    <article class="careerArticle">
                        <h2>EDUCATIONALIST & ADMINISTRATOR</h2>
                        <ul class="listItems">
                            <li>Former Principal, Govt. Degree College, Jagtial, AP</li>
                            <li>Former Principal, S.R.R. Govt. Degree College, Karimnagar, Telengana</li>
                            <li>Former Founder Principal, Women's College, Karimnagar</li>
                            <li>Former Principal, Government City College, Hyderabad</li>
                            <li>Former Coordinator, NTR Telugu University</li>
                            <li>Former Special Officer, Institute of Professional Studies, constituted by N.T. Rama Rao</li>
                            <li>Former Director, Satavahana Institute of Post Graduate Studies, Karimnagar</li>
                            <li>Former member, The Syndicate of Osmania University, Hyderabad</li>
                            <li>Former Joint Director, Higher Education, Govt. of Andhra Pradesh</li>
                            <li>Former Director, Telugu Academy, Andhra Pradesh</li>
                            <li>Former Founder Chairman, Sister Nivedita College of Professional Studies, Hyderabad</li>
                            <li>Founder Hon. Chairman, Telangana Literary, Educational and Cultural Forum</li>
                            <li>Founder Chairman, Telugu Bhasha Parirakshana Samithi, Telengana</li>
                            <li>Currently Hon. Chairman, Sister Nivedita School, Hyderabad</li>                            
                        </ul>
                    </article>

                    <article class="careerArticle">
                        <h2>JOURNALIST</h2>
                        <ul class="listItems">
                            <li>Former Editor - Satavahana journal of Satavahana Institute of Post Graduate Studies, Karimnagar</li>
                            <li>Former Principal, S.R.R. Govt. Degree College, Karimnagar, Telengana</li>
                            <li>Former Editor - Telugu monthly of Telugu Academy, Andhra Pradesh</li>
                            <li>Current Editor - Jayanthi, Telugu Quarterly</li>
                            <li>Former Editor - Business Vision, English</li>                           
                        </ul>
                    </article>

                    <article class="careerArticle">
                        <h2>HON. POSITIONS HELD</h2>
                        <ul class="listItems">
                            <li>Former President of the Poetry Society of Hyderabad</li>
                            <li>Founder President of the Inter-lingual Poetry Society of Hyderabad</li>
                            <li>Founder Convener of Telangana Educational and Cultural Forum, Hyderabad</li>
                            <li>Permanent Member of the Poetry Society of India, Delhi</li>
                            <li>Permanent Member of the Indian Institute of Public Administration, Delhi</li>
                            <li>Former Vice Chairman of the Forum for Higher Education, Hyderabad</li>
                            <li>Currently Chairman of the Vishwanatha Sahithya Peetam, Hyderabad</li>
                        </ul>
                    </article>
                </div>
                <!--/ col -->
            </div>
        </div>
        <!--/ container -->
    </div>
    <!--/ career -->

    <!-- books -->
    <div class="booksVelchala">
        <!-- container -->
        <div class="container">
             <article>
                <h1>Books by <br>Velchala Kondal Rao</h1>
                <p class="fwhite">36 Books written in Telugu, English, Urdu</p>                        
            </article>
            <!-- books swiper -->
                <div class="booksSwiper">
                    <!-- Swiper -->
                    <div class="swiper-container booksList">
                        <div class="swiper-wrapper">
                            <?php 
                            for ($i=0; $i<count($bookSlider);$i++){ ?>
                            <div class="swiper-slide">
                                <img src="img/books/<?php echo $bookSlider[$i][0]?>.jpg" alt="" class="img-fluid"> 
                            </div>
                            <?php } ?>                                               
                        </div>
                        <!-- Add Pagination -->
                        <!-- <div class="swiper-pagination"></div> -->
                        <!-- Add Arrows -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                     <p class="pt-5 text-center">
                        <a href="http://shop.velchala.com/" class="orangeBtn" target="_blank">View All Books</a>
                    </p>   
                </div>
                <!--/ books swiper -->
        </div>
        <!--/ container -->
    </div>
    <!--/ books -->

    <!-- video -->
    <div class="videoSection py-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <iframe frameborder="0" width="100%" height="600px" src="https://youtube.com/embed/WRBTxGCYxHo?autoplay=1&amp;mute=1&amp;loop=1&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;autohide=1&amp;playlist=WRBTxGCYxHo" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--/ video -->
    <!--/ main -->

    <!-- footer -->
    <footer>
        <ul class="nav justify-content-center socialnav">
            <li class="nav-item">
                <a href="https://www.facebook.com/" target="_blank" class="nav-link"><span class="icon-facebook icomoon"></span></a>
            </li>
            <li class="nav-item">
                <a href="https://twitter.com/" target="_blank" class="nav-link"><span class="icon-twitter icomoon"></span></a>
            </li>
            <li class="nav-item">
                <a href="https://www.linkedin.com/" target="_blank" class="nav-link"><span class="icon-linkedin icomoon"></span></a>
            </li>
        </ul>
        <p>© 2021 Velchala Kondal Rao. All rights reserved.</p>
    </footer>
    <!--/ footer -->
    <?php include 'includes/footer.php'?>   
</body>
</html>
